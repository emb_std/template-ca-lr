<!--
SPDX-FileCopyrightText: 2021 embeach

SPDX-License-Identifier: MIT OR Apache-2.0 OR CC-BY-4.0 OR GPL-3.0-or-later

Note-PCRLT:compliant-with-spec:SPDXv2.2:https://spdx.github.io/spdx-spec/
Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
Note-PCRLT:compliant-with-spec:PCRLTv0.2:https://gitlab.com/emb_std/pcrlt

Note-PCRLT:src-trace-repository:2021:public:git+https://gitlab.com/emb_std/template-ca-lr.git@master#LICENSE_README.md

Note-PCRLT:emb.7:specify-identity-name:embeach
Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/template-ca-lr.git@master#LICENSE_README.md
Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits regarding LICENSE_README.md

Note-PCRLT:MIT OR Apache-2.0 OR CC-BY-4.0 OR GPL-3.0-or-later:map-license-to-git-commits:all commits regarding LICENSE_README.md
-->
# License
## 1 Summary
The main license of this repository is MIT[^mit-license] (Free and Open Source; for the full text see [LICENSE.txt](LICENSE.txt)).  
Most of the files are MIT-licensed or even more permissive.  
Some files (ca. 8 files) are CC-BY-ND-4.0[^cc-by-nd-4] licensed (not OSI or FSF approved) because they are normative (e.g. licenses) and for this reason have stronger restrictions regarding modifications.  
Licenses and contents are associated on a per file basis.  
The SPDX document [LICENSE.spdx](LICENSE.spdx) includes all copyright and license information ("bill of materials"[^bill-of-materials]).  
To simplify the reuse of the components and remove any obstacles, the licensee can also choose a main license different from MIT.  
Most of the files are multi-licensed under MIT[^mit-license], Apache-2.0[^apache-license], (CC-BY-4.0[^cc-by-4]) and GPL-3.0-or-later[^gpl-3-or-later] (i.e. the licensee can choose the license).  
Some options for the concluded package license (using the "semicolon operator"[^semicolon-operator] and the "compatibility operator"[^compatibility-operator]) are:  
CC-BY-ND-4.0;<MIT  
CC-BY-ND-4.0;<Apache-2.0  
CC-BY-ND-4.0;<GPL-3.0-or-later  
CC-BY-ND-4.0;CC-BY-4.0;<MIT  
Some options as pure SPDX license expressions are:  
CC-BY-ND-4.0 AND CC0-1.0 AND (MIT OR Apache-2.0 OR GPL-3.0-or-later)  
CC-BY-ND-4.0 AND CC0-1.0 AND CC-BY-4.0 AND (MIT OR Apache-2.0 OR GPL-3.0-or-later)  
The PackageLicenseConcluded in the LICENSE.spdx document is: **"CC-BY-ND-4.0 AND MIT"** (It should be interpreted as a simplified option. Most projects would simplify the expression even further to **"MIT"** since the license texts (and similar files) are usually excluded when licensing the repository content.)  
The Project is SPDXv2.2, REUSEv3.0 and PCRLTv0.1 compliant.(see SPDX[^spdx],REUSE[^reuse],PCRLT[^pcrlt])  
For important details regarding copyright, licensing, relicensing, merging, read the following sections.
## 2 Some Basic Terms and Concepts
See [LICENSE_README_BASICS.md](LICENSE_README_BASICS.md), which should be equal to: <https://gitlab.com/emb_std/template-ca-lr/-/blob/v0.3.1/LICENSE_README_BASICS.md> (SHA1: c6d41a1559c2131351e1d222a14e3f79f323f3da)
## 3 Details 
**Disclaimer**: **To date, there are no lawyers among the authors of this document. Therefore, NOTHING in this document should be interpreted as legal advice. Use the given information at your own risk. Although the authors have tried very hard to research and compare available options regarding copyright and licensing compliance, they may be completely wrong!!! Be warned!!!**

* **3.1 REUSE compliance**: This repository is compliant with fsfe REUSE[^reuse] spec 3.0. The "fsfe-reuse tool"[^reuse-tool] can be used to verify this (see README.md).
    * **3.1.1 Copyright and License information per file**: This means that copyright holder lists and links to license files are specified on a per file basis. This is done either in the file header, an attached *.license file or a [.reuse/dep5](.reuse/dep5) specification.
    * **3.1.2 SPDX compliance and SPDX file (all copyright and license information)**: The SPDX[^spdx] Spec. 2.2 compliant SPDX file ("bill of materials"[^bill-of-materials]) from the last package or snapshot version is named LICENSE.spdx in this repository - it contains auto-generated content (especially the copyright and license information for every file) as well as manual additions. (For further details on how the file can be generated see README.md.)
    * **3.1.3 LICENSES folder**: The licenses linked in the files are included in the LICENSES folder (with SPDX and REUSE naming conventions). Note that files may be multi-licensed (i.e. the SPDX license expression contains an "OR"). If this is the case, the licensee can choose one license and remove the other license references from the files in question. If a license is no longer referenced by any file, it can (and should) be removed from the LICENSES folder to simplify the situation. If the licensee keeps the licenses they should be adhered to.
* **3.2 Compliance with PCRLT**: The repository is compliant with PCRLT[^pcrlt] (Privacy-aware Content, Rights Holder and License Tracing).
* **3.3 Scope of the License**: For each license in the LICENSES folder, the scope of the license should be interpreted as narrowly as possible. It should cover only those files or portions of files or Git changes for which this License has been specified by the respective copyright holder or in accordance with any licenses or agreements granted by that copyright holder. This narrow interpretation may be extended only if it would otherwise violate the terms of the license.
    * **3.3.1 Example MIT**: E.g., the term "Software" in the MIT license should only include those contents that have been linked to this license file (e.g., by the SPDX file header license identifier) and not contents that have been linked to another license file only. (Note that there may be license files with the same terms but different copyright holders to attribute.)
    * **3.3.2 Example GPLv3**: E.g. For GPLv3 (which might or might not currently be included in this repository), the approach of limiting the scope to content explicitly licensed under that license is probably not permissible, since the terms could be explicitly designed to affect other parts that have been incorperated.
* **3.4 Attribution requirement in the MIT license**: 
    * The LICENSES/MIT.txt file contains very generic copyright holder information. This is because, since copyright holers are already specified on a per file basis, it could lead to confusion about which copyright holders should be associated with the content. Therefore to "render" the generic expression in the license file, the SPDX file or the fsfe-reuse tool can be used (see 3.1.2). 
    * For copyright holders who may not have consented to generic attribution in the license file, an individual license file is used (listing only them). E.g. LICENSES/MIT_\<copyrightholder\>.txt
    * To give consent to generic attribution, the PCRLT[^pcrlt] (section 5.3) license file policy can be used in content files.
    * If Rights holders permit generic attribution in license files, but want to enforce that file-based copyright notices cannot be removed in any jurisdictions, they may need to include the license text in the file headers (but this is not recommended and causes confusion when multiple licenses are used in a file).
* **3.5 As a licensee, leave the copyright notices intact**: As a licensee, to comply with best practices, leave the file-based copyright notices intact (if the content is not completely replaced), even if a licensee might be allowed to remove them from MIT-only licensed files (assuming no verbatim license text is included in that file). Then the files can be incorperated in other projects quite safely, provided the licenses are compatible. Under other licenses, it might be explicitly required not to remove the copyright notices. Some (or possibly most) jurisdictions prohibit the removal anyway (e.g., 17 U.S. Code § 1202).
* **3.6 Regarding Git versions and Licenses**: Every commit defines a version of possibly copyrighted content. If a commit is part of a public online git repository the corresponding version is publicly acceessible. To make copyrighted content accessible to other users the agreement of copyright holder or a valid license is needed. Licenses mostly contain conditions. If those conditions are not complied to, the license is invalid and a copyright infringement has been constitued. This means that the git history could possibly contain copyright infringements. To remove a copyright infringement from the git history the history has to be removed or rewritten. This can have a huge impact on the project. However, a copyright holder does not have to tolerate any infringements. To keep the history but reduce the risk of the propagation of possible copyright issues from the git history, licensees should take care of the following:
    * Developers/Licensees should allways build on the last release or the current version and should not rely on old git versions of the git history to determine copyright holders and licenses - the history might contain license issues that have already been fixed in the current version. 
    * To protect licensees from license issues that are still in Git history, those issues should be listed in the .COPYRIGHT_ERRATA file.
* **3.7 Who is responsible for correct copyright and license information?**: 
By accepting the [Contributor Agreement](CONTRIBUTOR_AGREEMENT.md) the contributors take responsibility for their contributions and add evidence to the chain of trust. They promise to take best effort to determine the sources, copyright holders, and license terms of their contributions as well as specify them on a per file basis. This does not necessarily mean that they are liable for some misdeclaration of copyright or misdeclaration of license. Especially not, if they have declared the public available source and the copyright infringement was not detectable by taking reasonable efforts. In other cases they might be responsible for copyright infringement.
## References
[^reuse]:The fsfe REUSE project: <https://reuse.software>
[^reuse-tool]:fsfe-reuse tool: <https://git.fsfe.org/reuse/tool>
[^mit-license]: MIT License: <https://spdx.org/licenses/MIT.html>
[^apache-license]:Apache 2.0 - License: <https://spdx.org/licenses/Apache-2.0.html>
[^cc-by-4]:Creative Commons Attribution 4.0 International - License (CC-BY-4.0): <https://spdx.org/licenses/CC-BY-4.0.html>
[^cc-by-nd-4]:Creative Commons Attribution No Derivatives 4.0 International - License (CC-BY-ND-4.0): <https://spdx.org/licenses/CC-BY-ND-4.0.html>
[^gpl-3-or-later]:GNU General Public License v3.0 or later - License (GPL-3.0-or-later): <https://spdx.org/licenses/GPL-3.0-or-later.html>
[^spdx]:SPDX (Software Package Data Exchange): <https://spdx.dev>
[^bill-of-materials]:SPDX "bill of materials": <https://wiki.spdx.org/view/SPDX_FAQ>
[^pcrlt]:PCRLT (Privacy-aware Content, Rights Holder and License Tracing): <https://gitlab.com/emb_std/pcrlt>
[^semicolon-operator]: The semicolon-operator i.e. "A;B" currently means that for two licenses A and B there are some contents licensed under A and some other contents licensed under B. Where "A AND B" could mean the same but could also mean that they apply to the same files. This can make a difference e.g. for the reusability of contents. The idea was mentioned in: <https://github.com/spdx/spdx-spec/issues/123#issuecomment-496305203> . The operator is currently not allowed in SPDX (SPDXv2.2).
[^compatibility-operator]: The "compatibility-operator" (working name) can be used to simplify a license expression. E.g. the expression "CC-BY-ND-4.0;CC0-1.0;MIT" can be shortened to "CC-BY-ND-4.0;<MIT" and it reads as "the repository contains contents under CC-BY-ND-4.0 and other contents under MIT or compatible licenses. The operator is currently not allowed in SPDX (SPDXv2.2). A graph of compatibility between popular open source licenses was published here: <https://dwheeler.com/essays/floss-license-slide.html> and here: <https://commons.wikimedia.org/wiki/File:GPL-Compatible.svg> . Note that compatibility can have multiple dimensions and "the most helpful simplifications" is still a research topic.
