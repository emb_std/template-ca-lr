SPDXVersion: SPDX-<SPDXVersion>
DataLicense: CC0-1.0
SPDXID: SPDXRef-DOCUMENT
DocumentName: <PackageName>-<PackageVersion>
DocumentNamespace: http://spdx.org/spdxdocs/spdx-v<SPDXVersion>-<UUID>
Creator: Person: <SpdxCreatorPerson>
Creator: Tool: <SpdxCreatorTool>
Created: <Created>
CreatorComment: <text>This document (LICENSE.spdx) was semi-automatically generated using the tool chain from gitlab.com/emb_std/spdx-utils which makes use of the PCRLT tool from gitlab.com/emb_std/pcrlt-validator with backup of the REUSE tool. Afterwards it was manually adjusted/enriched. The tool was executed with the following relevant options:<PcrltValidatorExecutionOptions> (i.e. if a file is targeted by multiple PCRLT/REUSE file headers, the dep5 file header will take precedence)</text>

PackageName: <PackageName>
SPDXID: SPDXRef-package-<PackageName>
PackageVersion: <PackageVersion>
PackageFileName: <PackageName>-<PackageVersion>.zip
PackageDownloadLocation: git+https://gitlab.com/emb_std/template-ca-lr.git
PackageVerificationCode: <PackageVerificationCode> (excludes: <PackageVerificationCodeExcludedFiles>)
PackageLicenseConcluded: CC-BY-ND-4.0 AND MIT
PackageLicenseInfoFromFiles: Apache-2.0
PackageLicenseInfoFromFiles: CC0-1.0
PackageLicenseInfoFromFiles: CC-BY-4.0
PackageLicenseInfoFromFiles: CC-BY-ND-4.0
PackageLicenseInfoFromFiles: GPL-3.0-or-later
PackageLicenseInfoFromFiles: MIT
PackageLicenseDeclared: MIT
PackageLicenseComments: <text>The expression in PackageLicenseConcluded is the most simple option to choose from the licenses specified in the files (note that CC0-1.0 is not mentioned because it seems to have no obligations). The "AND" operator does not usually mean that the terms from all the combined licenses must be applied to every file in the package. Mostly it means that the files have different licenses and they can be applied separately. See LICENSE_README.md for further compliance assistance. The expression in PackageLicenseDeclared is the main license. See LICENSE_README.md for all options (example: PackageLicenseConcluded: CC-BY-ND-4.0 AND Apache-2.0).</text>
PackageCopyrightText: gitlab.com/emb_std/template-ca-lr contributors and possibly others



