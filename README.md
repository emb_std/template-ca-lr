<!--
SPDX-FileCopyrightText: 2021 embeach

SPDX-License-Identifier: MIT OR Apache-2.0 OR CC-BY-4.0 OR GPL-3.0-or-later

Note-PCRLT:compliant-with-spec:SPDXv2.2:https://spdx.github.io/spdx-spec/
Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
Note-PCRLT:compliant-with-spec:PCRLTv0.2:https://gitlab.com/emb_std/pcrlt

Note-PCRLT:src-trace-repository:2021:public:git+https://gitlab.com/emb_std/template-ca-lr.git@master#README.md

Note-PCRLT:emb.7:specify-identity-name:embeach
Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/template-ca-lr.git@master#README.md
Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits regarding README.md

Note-PCRLT:MIT OR Apache-2.0 OR CC-BY-4.0 OR GPL-3.0-or-later:map-license-to-git-commits:all commits regarding README.md
-->

# Repository template for using CONTRIBUTOR_AGREEMENT.md, LICENSE_README.md and LICENSE_README_BASICS.md
The repository provides some files that are intended to help with legal compliance for public open source software Git repositories.
It should be used as an example or template for using the following files:

* DCO.txt
* CONTRIBUTOR_AGREEMENT.md
* LICENSE_README.md
* LICENSE_README_BASICS.md

In a way compliant to:

* SPDX[^spdx]
* REUSE[^reuse]
* PCRLT[^pcrlt]
* and the used licenses

## Status
The repository is currently more of an idea of how things could be set up. It needs a lot more feedback and suggestions before it can serve as a recommendable solution.  
Use the package version tags to reference the exact version of the documents.
## Disclaimer
This is an experimental software project. Use it at your own risk.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
## Background/History
License management and compliance are difficult problems for which there is a lack of templates, best practices and standards, which hampers software development.  
The project was developed because no convincing template was found at the time of creation (2021-04).
## License
### Short Summary
The main license of this repository is MIT (Free and Open Source; for the full text see [LICENSE.txt](LICENSE.txt)).  
Licenses and contents are associated on a per file basis.  
The SPDX[^spdx] document [LICENSE.spdx](LICENSE.spdx) includes all copyright and license information ("bill of materials"[^bill-of-materials]).  
To use the templates without any obstacles, the licensee can also choose a main license different from MIT.  
Most of the files are multi-licensed under MIT[^mit-license], Apache-2.0[^apache-license], (CC-BY-4.0[^cc-by-4]) and GPL-3.0-or-later[^gpl-3-or-later] (i.e. the licensee can choose the license).  
The Project is SPDXv2.2, REUSEv3.0 and PCRLTv0.2 compliant.(see SPDX[^spdx],REUSE[^reuse],PCRLT[^pcrlt])  
For important details regarding copyright, licensing, relicensing, merging, see [LICENSE_README.md](LICENSE_README.md).
### Compliance with REUSE
The repository is compliant with REUSE[^reuse] where it is recommended to supply copyright and license information on a per file basis.  
After using the setup instructions from the "fsfe-reuse tool"[^reuse-tool] you can check the compliance with `reuse lint` or auto-generate a simple SPDX document with `reuse spdx`.  
Alternatively you can use the Makefile which uses the miniconda python environment. See [README_REUSE_tool.md](spdx-utils/README_REUSE_tool.md)  
### Generation and Verification of the SPDX document LICENSE.spdx
The supplied SPDX document [LICENSE.spdx](LICENSE.spdx) includes the information generated by `reuse spdx` and additional information.  
For further details on how the file can be generated and verified see [README_SPDX.md](spdx-utils/README_SPDX.md)
## Contributing
By committing and pushing as well as sending pull requests contributors agree to the [CONTRIBUTOR_AGREEMENT.md](CONTRIBUTOR_AGREEMENT.md) 1.1 (repository specific version). To increase the binding to this agreement contributors should sign-off their committs using `git commit -s`. 
## References
[^mit-license]:MIT License: <https://spdx.org/licenses/MIT.html>
[^reuse]:The fsfe REUSE project: <https://reuse.software>
[^reuse-tool]: The fsfe-reuse tool: <https://git.fsfe.org/reuse/tool>
[^apache-license]:Apache 2.0 License: <https://spdx.org/licenses/Apache-2.0.html>
[^cc-by-4]:Creative Commons Attribution 4.0 International - License (CC-BY-4.0): <https://spdx.org/licenses/CC-BY-4.0.html>
[^gpl-3-or-later]:GNU General Public License v3.0 or later - License (GPL-3.0-or-later): <https://spdx.org/licenses/GPL-3.0-or-later.html>
[^spdx]:SPDX (Software Package Data Exchange): <https://spdx.dev>
[^bill-of-materials]:SPDX "bill of materials": <https://wiki.spdx.org/view/SPDX_FAQ>
[^pcrlt]:PCRLT (Privacy-aware Content, Rights Holder and License Tracing): <https://gitlab.com/emb_std/pcrlt>
