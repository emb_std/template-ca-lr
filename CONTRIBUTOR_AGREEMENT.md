<!--
SPDX-FileCopyrightText: 2021 embeach

SPDX-License-Identifier: MIT OR Apache-2.0 OR CC-BY-4.0 OR GPL-3.0-or-later

Note-PCRLT:compliant-with-spec:SPDXv2.2:https://spdx.github.io/spdx-spec/
Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
Note-PCRLT:compliant-with-spec:PCRLTv0.2:https://gitlab.com/emb_std/pcrlt

Note-PCRLT:src-trace-repository:2021:public:git+https://gitlab.com/emb_std/template-ca-lr.git@master#CONTRIBUTOR_AGREEMENT.md

Note-PCRLT:emb.7:specify-identity-name:embeach
Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/template-ca-lr.git@master#CONTRIBUTOR_AGREEMENT.md
Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits regarding CONTRIBUTOR_AGREEMENT.md

Note-PCRLT:MIT OR Apache-2.0 OR CC-BY-4.0 OR GPL-3.0-or-later:map-license-to-git-commits:all commits regarding CONTRIBUTOR_AGREEMENT.md
-->

### Contributor Agreement
* 1 Repository specific Version of this document: 1.1
* 2 Date of last modification: 2021-04-25
* 3 Version policy: A different text needs a different version number. (Versions with posfix "SNAPSHOT" are an exception of this)
* 4 Applicable version: The applicable version for a contributor regarding a contribution is the most recent version (in the default branch and without "SNAPSHOT"-postfix) at the time of the contributing (e.g. pushing ,committing, sending pull a request).
* 5 Recommended SPDX license expression for Your contribution: "MIT OR Apache-2.0" (like the RUST language repository (<https://github.com/rust-lang/rust#license>))
* 6 By pushing (highest-responsibility-level) or committing (2nd-highest-responsibility-level) or sending pull requests (3nd-highest-responsibility-level) You (the contributor):
    * 6.1 agree to the "Developer Certificate of Origin" [DCO.txt](DCO.txt).
    * 6.2 agree that your contribution including your Git identity (name, email) will be made publicly available under the license provided.
    * 6.3 agree that the Git identity does not need to be removed from the Git history on demand in the future, as this would mean rewriting the Git history, which could cause a lot of trouble for the project, and that You therefore choose a Git identity (name and email) that may be kept in the public Git history (a pseudonymous Git identity is recommended, email addresses should work at the time of the contribution). The legal basis for the storage is the "Legitimate Interest" (Art. 6(1)(f) GDPR). 
    * 6.4 agree to have read LICENSE_README.md
    * 6.5 agree to take best efforts to comply with fsfe REUSEv3.0. See <https://reuse.software/faq>
    * 6.6 agree to take best efforts to comply with PCRLTv0.2. See <https://gitlab.com/emb_std/pcrlt>
    * 6.7 agree to leave file-based copyright notices from other copyright holders intact (unless the complete content is replaced)
    * 6.8 agree to take best efforts to determine the copyright holders that hold the copyright for the changes you want to contribute. (it might be of help to note why you think they are the copyright holder; if available add link to online source where the content is available)
    * 6.9 agree to add/update file-based copyright notices in all files that have changed in your contributed version and comply with the following details:
        * one REUSE-compliant copyright notice for each involved copyright holder.
        * if the change in a file is minimal you can ommit the copyright notice.
        * if the change in a file is major but is not protected by copyright you should add "Copyright NONE".
        * if you are rather unsure wether the content is protected by copyright (contains enough creativity/individuality) or not, treat it as protected.
        * consider also copyright holder that might hold ancillary copyrights (i.e. for simple fotos, databases etc.) or even patent rights.
        * to comply with GDPR: if the copyright holder is a natural person, do not write the copyright holder (or author) in content files without their permission. The permission can be coutiously assumed, if they have published those copyright notices already in public repositories to files you want to use (you should link to the source). If they are present only in the git metadata or somewhere else but not in the content files, permission can not be assumed and the names should be abbreviated and the repository to resolve the long form should be linked. Use the PCRLT notation for this.
        * map copyright holder to commits by using the PCRLT notation.
    * 6.10 agree to take best efforts to determine the licenses the copyright holder have made the content available to you or the licenses the copyright holder are willing to grant to you for the changes you want to contribute. (for copyright holders other than yourself, it might be of help to note why you are sure that they will grant this license; if available add link to online source where content/license/other evidences are available)
    * 6.11 agree to add/update file-based SPDX and REUSE compliant license notices in all files that have changed in your contributed version and comply with the following details:
        * one SPDX-compliant license notice for each snippet (commits are also treated as snippets in this context) that should be licensed under a different license expression. Use OR and AND operators to specify multi-licensing (in the sense that the licensee can choose) or to specify that multiple licenses should be complied to simultaniously. See SPDX and REUSE for more information. Map them to commits by using the PCRLT notation.
        * choose a license that is compatible with all other licenses in the project (see LICENSES). Please use the recommended license that is stated in section "5. Recommended SPDX license expression for Your contribution".
    * 6.12 should use `git commit -s` to "sign" your commit (i.e. adding the line "Signed-off by ..." - e.g. `Signed-off by johndoe <johndoe@example.com>`)  
