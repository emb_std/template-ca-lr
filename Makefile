# SPDX-FileCopyrightText: 2021 embeach
# 
# SPDX-License-Identifier: CC0-1.0 OR MIT OR Apache-2.0 OR GPL-3.0-or-later
# 
# Note-PCRLT:compliant-with-spec:SPDXv2.2:https://spdx.github.io/spdx-spec/
# Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
# Note-PCRLT:compliant-with-spec:PCRLTv0.3:https://gitlab.com/emb_std/pcrlt
# 
# Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/template-ca-lr.git@master#Makefile
# Note-PCRLT:src-trace-repository:2021:public:git+https://gitlab.com/emb_std/spdx-utils.git@master#Makefile
# 
# Note-PCRLT:emb.7:specify-identity-name:embeach
# Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/spdx-utils.git@master#Makefile
# Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits authored/committed by emb.7 regarding Makefile
# 
# Note-PCRLT:CC0-1.0 OR MIT OR Apache-2.0 OR GPL-3.0-or-later:map-license-to-git-commits:same as "emb.7:map-copyright-holder-to-git-commits"

download-spdx-utils-main:
	if [ -d "spdx-utils-main" ]; then echo "The folder already exists. A download does not seem to be necessary." && false ; fi
	git clone https://gitlab.com/emb_std/spdx-utils.git tmp_spdx-utils
	cp -a tmp_spdx-utils/spdx-utils-main spdx-utils-main
	rm -rf tmp_spdx-utils
