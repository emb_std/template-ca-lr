<!--
SPDX-FileCopyrightText: 2021 embeach

SPDX-License-Identifier: MIT OR Apache-2.0 OR CC-BY-4.0 OR GPL-3.0-or-later

Note-PCRLT:compliant-with-spec:SPDXv2.2:https://spdx.github.io/spdx-spec/
Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
Note-PCRLT:compliant-with-spec:PCRLTv0.2:https://gitlab.com/emb_std/pcrlt

Note-PCRLT:src-trace-repository:2021:public:git+https://gitlab.com/emb_std/template-ca-lr.git@master#LICENSE_README_BASICS.md

Note-PCRLT:emb.7:specify-identity-name:embeach
Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/template-ca-lr.git@master#LICENSE_README_BASICS.md
Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits regarding LICENSE_README_BASICS.md

Note-PCRLT:MIT OR Apache-2.0 OR CC-BY-4.0 OR GPL-3.0-or-later:map-license-to-git-commits:all commits regarding LICENSE_README_BASICS.md
-->
**Disclaimer**: **To date, there are no lawyers among the authors of this document. Therefore, NOTHING in this document should be interpreted as legal advice. Use the given information at your own risk. Although the authors have tried very hard to research and compare available options regarding copyright and licensing compliance, they may be completely wrong!!! Be warned!!!**
## 2. Some Basic Terms and Concepts
* **2.1 Jurisdictions**: The following applies to most jurisdictions. However, they may differ in detail.
* **2.2 Copyright protected content**: In the following, copyrighted content is referred to as protected content or simply content. Content may include, for example, text, images, audio, video, or graphical user interfaces. Protected content must have a fixed form, i.e. ideas are not protected. Violation of copyright laws related to a particular protected content is called copyright infringement (e.g. publishing in original or modified form without permission). 
* **2.3 Content with and without creative reference**: The legal reason for the protection of content can lie both in the protection of creative work (copyrights in the original/narrower sense) and in the protection of performances without a necessary creative reference (ancillary copyrights). 
* **2.4 Protection based on creative reference**: Content is protected if it contains a minimum level of human creativity. A workable assumption about the degree of creativity might be: "5 lines of code with 80 characters that other developers would write very differently are already protected". A software-generated text file may also be protected, for example, if it contains a substantial amount of protected content from source code, resources, or input. Whether and when AI-generated content is protected is unclear. 
* **2.5 No registration or copyright notice required**: Content is protected from the moment it is fixed (e.g. written down), without any further requirements.
* **2.6 Related Rights for Protection**: There are more rights for protection that are subsumed by copyright - e.g. protection of simple fotos, databases (ancillary copyright). Patents are coped differently - they will need registration.
* **2.7 Copyright holder**: The copyright holder of a content is usually the author. If the author is employed, the copyright will usually automatically move to the employer.
* **2.8 Joint works**: A content can include multiple authors/copyright holders. For licensing or transfering the rights, this usually means that all copyright holders must agree.
* **2.9 Licenses and transfer of Copyright**: The rights contained in the copyright can be transferred or granted in whole or in part, exclusively or nonexclusively. Granted rights are usually called licenses. (In some jurisdictions, some creator rights are not fully transferable).
* **2.10 Licensor, Licensee regarding a certain license and content**: The licensor is usually the copyright holder or someone who has been authorized by the copyright holder (e.g. by a license) to grant licenses. The licensee is the one who has been granted the rights. 
* **2.11 Free and Open Source Licenses**: Open source licenses cover use, inspection of source code and modification, publication of the original, and publication of modified versions - all for any purpose. Many open source licenses also cover patent rights. There are several organizations that make statements about whether they believe a license meets the required conditions. The most relevant are FSF[^fsf] and OSI[^osi].
* **2.12 License Conditions**: Licenses usually contain license conditions. These can be e.g. certain attributions or to provide the source code in case of a redistribution or to license derivative works under the same license.
* **2.13 Limited Liability/Warranty**: Licenses may contain liability/warranty disclaimer. Most open source licenses have this.
* **2.14 Multi-licensing (e.g. Dual-licensing)**: A content can be made available under several different licenses. The licensee can choose which license to apply to the content. However, if the licenses conflict, both cannot apply at the same time.
* **2.15 Re-licensing**: A content can be made available under a different license, if all copyright holders agree. I.e. they grant those rights or they might already have granted a license for relicensing.
* **2.16 Sub-Licensing**: The right to sub-licensing means usually that a content can be licensed under a different license, if this (sub)license does not violate the original license.
* **2.17 License and copyright information**: To ensure that no copyright or license infringement has occurred, it is important that for all pieces of content the correct copyright holder and license information are specified.
* **2.18 License compatibility and compliance**: To ensure that no copyright or license infringement has occurred, it is important not only that all copyright and license declarations are correct, but also that the licensing terms of the involved licenses are compatible and have been adhered to.
* **2.19 Example workflow regarding Open Source Licenses**: Suppose a developer clones a public online git repository R1 to her local storage and finds a file that contains a copyright notice (i.e. the declared copyright holder) and a license notice (i.e. which license the copyright holder grants) and attached to it the license text (e.g. in a separate file). Then she will assume that she (as licensee) has been granted a license by the declared copyright holders (the licensors) and can use the content in the limits of this license. She might want to include the file in some other public online git repository R2. She makes sure that the license does allow this and that the license is compatible with the licenses of R2 and contributes the file together with the license to R2 (by committing and pushing). Before contributing she will look for any necessary or implied Contributor Agreements (e.g. DCO,CLA) of R2 or any other thigs that might get triggered by contributing.  
    * **2.19.1 Normally this will work but here are some things that can go wrong**: A copyright holder might complain and claim that there is copyright infringement in R2 because:(one of)
        1. The Copyright notice in the file header in R2 is wrong. 
        2. The License notice in the file header in R2 is wrong.
        3. The License text related to the file in R2 is wrong.
        4. The licenses are incompatible.
        5. The license conditions have not been complied with.
    * **2.19.2 What could be the consequences?** If this is true, the error might have happend during a change in R1 or during the transfer to R2 or after the transfer to R2. Usually the git history can be used to trace where the error was firstly introduced. But if the git history was rewritten this is no longer reliable. The error could get fixed (e.g., by removing the content) in the current version of the R2 repository (and possibly in R1), but then the copyright infringement would still exist in the earlier Git version (because it is publicly accessible and checking out that version will still contain the infringement). The copyright holder can insist to enforce their copyrights and the content has to be removed from the git history by rewriting the git history. The copyright holder can also demand compensation if this is a more serious issue. It is not clear who can be liable for the infringement. The maintainer of R2 will argue, that the contribution was user generated and the infringing contributor will have to take the responcibility. If the contributor has done the infringement intentionally he/she might be liable. If he/she has done it accidently he/she might be liable too. If the error was already in R1 then he/she is probably not liable but the copyright holder can try to find someone who is liable under the contributors of R1. If the maintainers of R2 are not willing to react, the hoster of R2 can be requested via notice and takedown policy (the hoster can block public access).

### References
[^fsf]: Free Software Foundation: <https://www.fsf.org/>
[^osi]: Open Source Initiative: <https://opensource.org/>
